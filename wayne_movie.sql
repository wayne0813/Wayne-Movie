/*
Navicat MySQL Data Transfer

Source Server         : Wayne-Aliyun
Source Server Version : 50724
Source Host           : 47.99.246.15:3306
Source Database       : wayne_movie

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-01-06 21:27:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for movie
-- ----------------------------
DROP TABLE IF EXISTS `movie`;
CREATE TABLE `movie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `movie_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '电影名',
  `movie_director` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '导演',
  `area` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '拍摄地区',
  `year` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '拍摄年限',
  `img_path` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '封面图片',
  `simpo_intro` longtext CHARACTER SET utf8mb4 COMMENT '精简简介',
  `movie_intro` longtext CHARACTER SET utf8mb4 COMMENT '电影简介',
  `page_view` int(11) DEFAULT '0' COMMENT '浏览量',
  `reply_num` int(11) DEFAULT '0' COMMENT '回复量',
  `is_delete` char(1) CHARACTER SET utf8mb4 DEFAULT '0' COMMENT '是否删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1012 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='电影表';

-- ----------------------------
-- Records of movie
-- ----------------------------
INSERT INTO `movie` VALUES ('1001', '蜘蛛侠：平行宇宙', 'Wayne', '美国', '2018-12-14', 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2542867516.webp', '迈尔斯（沙梅克·摩尔 配音）的父亲是一位一板一眼的警官，而他的母亲则是一名工作勤奋的护士。慈爱的父母对于孩子的成就非常自豪，也希望他能够融入新加入的这所优秀的学校，在这里取得成功。然而迈尔斯的生活因为一次意外变得更加复杂……', '迈尔斯（沙梅克·摩尔 配音）的父亲是一位一板一眼的警官，而他的母亲则是一名工作勤奋的护士。慈爱的父母对于孩子的成就非常自豪，也希望他能够融入新加入的这所优秀的学校，在这里取得成功。然而迈尔斯的生活因为一次意外变得更加复杂。他被一只放射性蜘蛛咬伤，并因此获得了毒液冲击、伪装隐藏、蜘蛛爬行、超凡听力、蜘蛛感应等一系列超能力。与此同时，这座城市里最臭名昭着的犯罪头目金并（列维·施瑞博尔 配音）已经建立起一台高度隐秘的超级对撞机，这台对撞机开启了通往其他宇宙的时空通道，来自其他宇宙、不同版本的蜘蛛侠（包括中年彼得·帕克、女蜘蛛侠格温、暗影蜘蛛侠、蜘猪侠和潘妮·帕克）也来到了迈尔斯所在的世界。在这些新老角色的帮助下，迈尔斯慢慢学习、逐渐接受挑战，也学会了作为一名超级英雄所要承担的责任。他最终意识到，任何人都可以戴上超级英雄的面具，为正义而战……', '282', '8', '0', '2018-12-26 09:57:49', '2019-01-04 16:00:07', null);
INSERT INTO `movie` VALUES ('1002', '海王', 'Wayne', '澳大利亚', '2018-12-21', 'https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2541280047.webp', '华纳兄弟影片公司与导演温子仁联手为您呈现波澜壮阔的动作冒险电影——《海王》！横跨七大洋的广阔海底世界徐徐展开，给观众带来震撼十足的视觉奇观。本片由杰森·莫玛领衔主演，讲述半人半亚特兰蒂斯血统的亚瑟·库瑞踏上永生难忘的征途——他不但需要直面自己的特殊身世......', '华纳兄弟影片公司与导演温子仁联手为您呈现波澜壮阔的动作冒险电影——《海王》！横跨七大洋的广阔海底世界徐徐展开，给观众带来震撼十足的视觉奇观。本片由杰森·莫玛领衔主演，讲述半人半亚特兰蒂斯血统的亚瑟·库瑞踏上永生难忘的征途——他不但需要直面自己的特殊身世，更不得不面对生而为王的考验：自己究竟能否配得上“海王”之名。', '128', '0', '0', '2018-12-26 10:02:03', '2018-12-27 18:45:37', null);
INSERT INTO `movie` VALUES ('1003', '我不是药神', 'Wayne', '中国大陆', '2018-07-05', 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2519070834.webp', '普通中年男子程勇（徐峥 饰）经营着一家保健品店，失意又失婚。不速之客吕受益（王传君 饰）的到来，让他开辟了一条去印度买药做“代购”的新事业，虽然困难重重，但他在这条“买药之路”上发现了商机，一发不可收拾地做起了治疗慢粒白血病的印度仿制药独家代理商......', '普通中年男子程勇（徐峥 饰）经营着一家保健品店，失意又失婚。不速之客吕受益（王传君 饰）的到来，让他开辟了一条去印度买药做“代购”的新事业，虽然困难重重，但他在这条“买药之路”上发现了商机，一发不可收拾地做起了治疗慢粒白血病的印度仿制药独家代理商。赚钱的同时，他也认识了几个病患及家属，为救女儿被迫做舞女的思慧（谭卓 饰）、说一口流利“神父腔”英语的刘牧师（杨新鸣 饰），以及脾气暴烈的“黄毛”（章宇 饰），几个人合伙做起了生意，利润倍增的同时也危机四伏。程勇昔日的小舅子曹警官（周一围 饰）奉命调查仿制药的源头，假药贩子张长林（王砚辉 饰）和瑞士正牌医药代表（李乃文 饰）也对其虎视眈眈，生意逐渐变成了一场关于救赎的拉锯战。 \r\n　　本片改编自慢粒白血病患者陆勇代购抗癌药的真实事迹。', '956', '0', '0', '2018-12-27 18:35:35', '2018-12-27 18:45:04', null);
INSERT INTO `movie` VALUES ('1004', '西虹市首富', 'Wayne', '中国大陆', '2018-07-27', 'https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2529206747.webp', '西虹市丙级球队大翔队的守门员王多鱼（沈腾 饰）因比赛失利被教练开除，一筹莫展之际王多鱼突然收到神秘人士金老板（张晨光 饰）的邀请，被告知自己竟然是保险大亨王老太爷（李立群 饰）的唯一继承人，遗产高达百亿！但是王老太爷给出了一个非常奇葩的条件......', '西虹市丙级球队大翔队的守门员王多鱼（沈腾 饰）因比赛失利被教练开除，一筹莫展之际王多鱼突然收到神秘人士金老板（张晨光 饰）的邀请，被告知自己竟然是保险大亨王老太爷（李立群 饰）的唯一继承人，遗产高达百亿！但是王老太爷给出了一个非常奇葩的条件，那就是要求王多鱼在一个月内花光十亿，还不能告诉身边人，否则失去继承权。王多鱼毫不犹豫签下了“军令状”，与好友庄强（张一鸣 饰）以及财务夏竹（宋芸桦 饰）一起开启了“挥金之旅”，即将成为西虹市首富的王多鱼，第一次感受到了做富人的快乐，同时也发现想要挥金如土实在没有那么简单！', '32', '0', '0', '2018-12-27 18:36:59', '2018-12-27 18:45:08', null);
INSERT INTO `movie` VALUES ('1005', '夏洛特烦恼', 'Wayne', '中国大陆', '2015-09-30', 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2264377763.webp', '在学生时代的初恋秋雅（王智 饰）的婚礼上，毕业后吃软饭靠老婆养的夏洛（沈腾 饰）假充大款，出尽其丑，中间还被老婆马冬梅（马丽 饰）戳穿暴捶。混乱之中，夏洛意外穿越时空，回到了1997年的学生时代的课堂里。他懵懵懂懂，以为是场真实感极强的梦......', '在学生时代的初恋秋雅（王智 饰）的婚礼上，毕业后吃软饭靠老婆养的夏洛（沈腾 饰）假充大款，出尽其丑，中间还被老婆马冬梅（马丽 饰）戳穿暴捶。混乱之中，夏洛意外穿越时空，回到了1997年的学生时代的课堂里。他懵懵懂懂，以为是场真实感极强的梦，于是痛揍王老师，强吻秋雅，还尝试跳楼让自己醒来。当受伤的他从病床上苏醒时，他意识到自己真的穿越了时空。既然有机会重新来过，那不如好好折腾一回。他勇敢追求秋雅、奚落优等生袁华（尹正 饰）、拒绝马冬梅的死缠烂打。后来夏洛凭借“创作”朴树、窦唯等人的成名曲而进入娱乐圈。 \r\n　　他的人生发生翻天覆地的巨变，但是内心某个地方却越来越感到空虚……', '64', '0', '0', '2018-12-27 18:37:01', '2018-12-27 18:45:10', null);
INSERT INTO `movie` VALUES ('1006', '功夫 ', 'Wayne', '中国大陆 / 香港', '2004-12-23', 'https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2219011938.webp', '1940年代的上海，自小受尽欺辱的街头混混阿星（周星驰）为了能出人头地，可谓窥见机会的缝隙就往里钻，今次他盯上行动日益猖獗的黑道势力“斧头帮”，想借之大名成就大业。 \r\n　　阿星假冒“斧头帮”成员试图在一个叫“猪笼城寨”的地方对居民敲诈......', '1940年代的上海，自小受尽欺辱的街头混混阿星（周星驰）为了能出人头地，可谓窥见机会的缝隙就往里钻，今次他盯上行动日益猖獗的黑道势力“斧头帮”，想借之大名成就大业。 \r\n　　阿星假冒“斧头帮”成员试图在一个叫“猪笼城寨”的地方对居民敲诈，不想引来真的“斧头帮”与“猪笼城寨”居民的恩怨。“猪笼城寨”原是藏龙卧虎之处，居民中有许多身怀绝技者（元华、梁小龙等），他们隐藏于此本是为远离江湖恩怨，不想麻烦自动上身，躲都躲不及。而在观战正邪两派的斗争中，阿星逐渐领悟功夫的真谛。', '435', '0', '0', '2018-12-27 18:37:02', '2018-12-27 18:45:16', null);
INSERT INTO `movie` VALUES ('1007', '愤怒的黄牛', 'Wayne', '韩国', '2018-11-22', 'https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2538593487.webp', '过去曾是黑帮成员的东哲，努力洗清黑暗的过去，和天使般善良的妻子智秀过着平凡的生活。某天妻子智秀遭到绑架。不久，一通陌生来电表示要付东哲一笔钱，要求东哲将妻子卖给他…东哲将用行动证明绑匪惹错了人！', '过去曾是黑帮成员的东哲，努力洗清黑暗的过去，和天使般善良的妻子智秀过着平凡的生活。某天妻子智秀遭到绑架。不久，一通陌生来电表示要付东哲一笔钱，要求东哲将妻子卖给他…东哲将用行动证明绑匪惹错了人！', '12', '0', '0', '2018-12-27 18:37:03', '2018-12-27 18:45:19', null);
INSERT INTO `movie` VALUES ('1008', '摔跤吧！爸爸', 'Wayne', '印度', '2017-05-05', 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2457983084.webp', '马哈维亚（阿米尔·汗 Aamir Khan 饰）曾经是一名前途无量的摔跤运动员，在放弃了职业生涯后，他最大的遗憾就是没有能够替国家赢得金牌。马哈维亚将这份希望寄托在了尚未出生的儿子身上，哪知道妻子接连给他生了两个女儿，取名......', '马哈维亚（阿米尔·汗 Aamir Khan 饰）曾经是一名前途无量的摔跤运动员，在放弃了职业生涯后，他最大的遗憾就是没有能够替国家赢得金牌。马哈维亚将这份希望寄托在了尚未出生的儿子身上，哪知道妻子接连给他生了两个女儿，取名吉塔（法缇玛·萨那·纱卡 Fatima Sana Shaikh 饰）和巴比塔（桑亚·玛荷塔 Sanya Malhotra 饰）。让马哈维亚没有想到的是，两个姑娘展现出了杰出的摔跤天赋，让他幡然醒悟，就算是女孩，也能够昂首挺胸的站在比赛场上，为了国家和她们自己赢得荣誉。 \r\n　　就这样，在马哈维亚的指导下，吉塔和巴比塔开始了艰苦的训练，两人进步神速，很快就因为在比赛中连连获胜而成为了当地的名人。为了获得更多的机会，吉塔进入了国家体育学院学习，在那里，她将面对更大的诱惑和更多的选择。', '367', '0', '0', '2018-12-27 18:37:03', '2018-12-27 18:45:25', null);
INSERT INTO `movie` VALUES ('1009', '阿甘正传', 'Wayne', '美国', '1994-06-23', 'https://img1.doubanio.com/view/photo/s_ratio_poster/public/p510876377.webp', '阿甘（汤姆·汉克斯 饰）于二战结束后不久出生在美国南方阿拉巴马州一个闭塞的小镇，他先天弱智，智商只有75，然而他的妈妈是一个性格坚强的女性，她常常鼓励阿甘“傻人有傻福”，要他自强不息。 \r\n　　阿甘像普通孩子一样上学，并且认识了一生的朋友和至爱珍妮......', '阿甘（汤姆·汉克斯 饰）于二战结束后不久出生在美国南方阿拉巴马州一个闭塞的小镇，他先天弱智，智商只有75，然而他的妈妈是一个性格坚强的女性，她常常鼓励阿甘“傻人有傻福”，要他自强不息。 \r\n　　阿甘像普通孩子一样上学，并且认识了一生的朋友和至爱珍妮（罗宾·莱特·潘 饰），在珍妮和妈妈的爱护下，阿甘凭着上帝赐予的“飞毛腿”开始了一生不停的奔跑。 \r\n　　阿甘成为橄榄球巨星、越战英雄、乒乓球外交使者、亿万富翁，但是，他始终忘不了珍妮，几次匆匆的相聚和离别，更是加深了阿甘的思念。 \r\n　　有一天，阿甘收到珍妮的信，他们终于又要见面……', '864', '0', '0', '2018-12-27 18:37:04', '2018-12-27 18:45:28', null);
INSERT INTO `movie` VALUES ('1010', '肖申克的救赎', 'Wayne', '美国', '1994-09-10', 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p480747492.webp', '0世纪40年代末，小有成就的青年银行家安迪（蒂姆·罗宾斯 Tim Robbins 饰）因涉嫌杀害妻子及她的情人而锒铛入狱。在这座名为肖申克的监狱内，希望似乎虚无缥缈，终身监禁的惩罚无疑注定了安迪接下来灰暗绝望的人生。未过多久，安迪尝试接近囚犯中颇有声望的瑞德......', '20世纪40年代末，小有成就的青年银行家安迪（蒂姆·罗宾斯 Tim Robbins 饰）因涉嫌杀害妻子及她的情人而锒铛入狱。在这座名为肖申克的监狱内，希望似乎虚无缥缈，终身监禁的惩罚无疑注定了安迪接下来灰暗绝望的人生。未过多久，安迪尝试接近囚犯中颇有声望的瑞德（摩根·弗里曼 Morgan Freeman 饰），请求对方帮自己搞来小锤子。以此为契机，二人逐渐熟稔，安迪也仿佛在鱼龙混杂、罪恶横生、黑白混淆的牢狱中找到属于自己的求生之道。他利用自身的专业知识，帮助监狱管理层逃税、洗黑钱，同时凭借与瑞德的交往在犯人中间也渐渐受到礼遇。表面看来，他已如瑞德那样对那堵高墙从憎恨转变为处之泰然，但是对自由的渴望仍促使他朝着心中的希望和目标前进。而关于其罪行的真相，似乎更使这一切朝前推进了一步…… \r\n　　本片根据著名作家斯蒂芬·金（Stephen Edwin King）的原著改编。', '845', '0', '0', '2018-12-27 18:37:04', '2018-12-27 18:45:31', null);
INSERT INTO `movie` VALUES ('1011', '当幸福来敲门', 'Wayne', '美国', '2008-01-17', 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1312700744.webp', '克里斯•加纳（威尔·史密斯 Will Smith 饰）用尽全部积蓄买下了高科技治疗仪，到处向医院推销，可是价格高昂，接受的人不多。就算他多努力都无法提供一个良好的生活环境给妻儿，妻子（桑迪·牛顿 Thandie Newton 饰）最终选择离开家。从此他带着儿子克里斯托夫......', '克里斯•加纳（威尔·史密斯 Will Smith 饰）用尽全部积蓄买下了高科技治疗仪，到处向医院推销，可是价格高昂，接受的人不多。就算他多努力都无法提供一个良好的生活环境给妻儿，妻子（桑迪·牛顿 Thandie Newton 饰）最终选择离开家。从此他带着儿子克里斯托夫（贾登·史密斯 Jaden Smith 饰）相依为命。克里斯好不容易争取回来一个股票投资公司实习的机会，就算没有报酬，成功机会只有百分之五，他仍努力奋斗，儿子是他的力量。他看尽白眼，与儿子躲在地铁站里的公共厕所里，住在教堂的收容所里…… 他坚信，幸福明天就会来临。', '657', '0', '0', '2018-12-27 18:44:21', '2018-12-27 18:45:33', null);

-- ----------------------------
-- Table structure for movie_reply
-- ----------------------------
DROP TABLE IF EXISTS `movie_reply`;
CREATE TABLE `movie_reply` (
  `reply_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `reply_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `reply_state` char(1) DEFAULT '0' COMMENT '评论状态',
  `movie_id` int(11) DEFAULT NULL COMMENT '电影id',
  `reviewer_id` int(11) DEFAULT NULL COMMENT '评论者',
  `reviewer_uid` int(11) DEFAULT '0' COMMENT '评论者评论的谁',
  `pid` int(11) DEFAULT NULL COMMENT '父评论',
  `is_delete` char(1) DEFAULT '0' COMMENT '是否删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8mb4 COMMENT='评论表';

-- ----------------------------
-- Records of movie_reply
-- ----------------------------
INSERT INTO `movie_reply` VALUES ('1001', '这是一条评论-01', '1', '1001', '1001', '0', '0', '0', '2018-12-29 19:33:39', '2018-12-30 12:23:37', null);
INSERT INTO `movie_reply` VALUES ('1002', '这是一条评论-02', '0', '1001', '1001', '0', '0', '0', '2018-12-29 19:35:40', '2018-12-30 12:13:32', null);
INSERT INTO `movie_reply` VALUES ('1003', '这是一条评论-1001-01', '0', '1001', '1002', '1001', '1001', '0', '2018-12-29 19:44:06', '2018-12-30 12:13:34', null);
INSERT INTO `movie_reply` VALUES ('1004', 'aaa', '0', '1001', null, '0', null, '0', '2019-01-03 19:41:50', '2019-01-03 19:41:50', null);

-- ----------------------------
-- Table structure for movie_type
-- ----------------------------
DROP TABLE IF EXISTS `movie_type`;
CREATE TABLE `movie_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `movie_id` int(11) DEFAULT NULL COMMENT '电影id',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='电影类型关系表';

-- ----------------------------
-- Records of movie_type
-- ----------------------------
INSERT INTO `movie_type` VALUES ('1', '1001', '1001');
INSERT INTO `movie_type` VALUES ('2', '1002', '1001');
INSERT INTO `movie_type` VALUES ('3', '1001', '1002');

-- ----------------------------
-- Table structure for system_admin
-- ----------------------------
DROP TABLE IF EXISTS `system_admin`;
CREATE TABLE `system_admin` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `admin_name` varchar(45) DEFAULT NULL COMMENT '管理员名字',
  `password` varchar(99) DEFAULT NULL COMMENT '管理员密码',
  `mobile` char(11) DEFAULT NULL COMMENT '管理员手机号',
  `is_delete` char(1) DEFAULT '0' COMMENT '是否删除 0-未删除 1-已删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8mb4 COMMENT='管理员表';

-- ----------------------------
-- Records of system_admin
-- ----------------------------
INSERT INTO `system_admin` VALUES ('1001', 'wayne', '123', null, '0', '2019-01-04 17:26:27', '2019-01-04 17:26:27', null);

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_name` varchar(255) DEFAULT NULL COMMENT '类型名称',
  `is_delete` char(1) DEFAULT '0' COMMENT '是否删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8mb4 COMMENT='类型表';

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES ('1001', '情怀', '0', '2018-12-26 13:31:41', '2018-12-26 13:31:41', null);
INSERT INTO `type` VALUES ('1002', '社会', '0', '2018-12-26 13:32:11', '2018-12-26 13:32:11', null);

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `mobile` char(11) DEFAULT NULL COMMENT '手机号',
  `is_delete` char(1) DEFAULT '0' COMMENT '是否删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1006 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1001', 'Test-01', 'test-01', '123', '111111', '0', '2018-12-29 19:47:24', '2019-01-04 10:23:05', null);
INSERT INTO `user_info` VALUES ('1002', 'Test-02', 'test-02', '123', '222222', '0', '2018-12-29 19:47:35', '2019-01-04 10:23:08', null);
INSERT INTO `user_info` VALUES ('1003', 'Test-03', 'test-03', '123', '333333', '0', '2018-12-29 19:47:44', '2019-01-04 10:23:10', null);
INSERT INTO `user_info` VALUES ('1004', '苏三', 'Wayne', '123', '777777', '0', '2019-01-04 13:33:24', '2019-01-04 13:33:24', null);
INSERT INTO `user_info` VALUES ('1005', 'test-05', 'test-05', '123', '555555', '0', '2019-01-04 13:43:25', '2019-01-04 13:43:25', null);
SET FOREIGN_KEY_CHECKS=1;
