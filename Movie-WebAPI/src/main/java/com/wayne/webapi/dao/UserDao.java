package com.wayne.webapi.dao;

import com.wayne.webapi.entity.UserInfo;
import com.wayne.webapi.util.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class UserDao {

    public UserInfo selectUserByMobileAndPassword(String mobile, String password) {
        String SQL = "select * from user_info where mobile = ? and password = ?";
        return DbHelp.query(SQL, new BeanHandler<>(UserInfo.class, new BasicRowProcessor(new GenerousBeanProcessor())), mobile, password);
    }

    public int saveNewUserInfo(UserInfo userInfo) {
        String SQL = "insert into user_info (user_name, nick_name, password, mobile) values (?,?,?,?)";
        return DbHelp.insert(SQL, new ScalarHandler<Long>(),userInfo.getUserName(), userInfo.getNickName(), userInfo.getPassword(), userInfo.getMobile()).intValue();
    }

    public UserInfo selectUserById(Integer id) {
        String SQL = "select * from user_info where user_id = ?";
        return DbHelp.query(SQL, new BeanHandler<>(UserInfo.class, new BasicRowProcessor(new GenerousBeanProcessor())), id);
    }
}
