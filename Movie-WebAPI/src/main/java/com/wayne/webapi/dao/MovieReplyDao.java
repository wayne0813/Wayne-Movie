package com.wayne.webapi.dao;

import com.wayne.webapi.entity.MovieReply;
import com.wayne.webapi.util.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.List;

/**
 * @author Wayne
 * @date 2018/12/29
 */
public class MovieReplyDao {
    public List<MovieReply> selectMovieReplyByMovieId(Integer movieId) {
        String SQL = "SELECT mr.*, ui.user_name AS 'reviewer_name' FROM movie_reply mr LEFT JOIN user_info ui ON mr.reviewer_id = ui.user_id WHERE mr.pid = 0 AND mr.reply_state = '1' AND mr.movie_id = ? AND mr.is_delete = '0' ORDER BY\tmr.create_time DESC";
        return DbHelp.query(SQL, new BeanListHandler<>(MovieReply.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieId);
    }

    public void insertMovieReply(String movieId, String reviewerId, String replyContent) {
        String SQL = "insert into movie_reply (movie_id, reviewer_id, reply_content) values (?, ?, ?)";
        DbHelp.update(SQL, movieId, reviewerId, replyContent);
    }
}
