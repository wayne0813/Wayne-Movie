package com.wayne.webapi.dao;

import com.wayne.webapi.entity.Type;
import com.wayne.webapi.util.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.List;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class TypeDao {

    public List<Type> selectAllType() {
        String SQL = "SELECT t.type_id, t.type_name, COUNT(t.type_name) AS remark FROM type t INNER JOIN movie_type mt ON t.type_id = mt.type_id WHERE t.is_delete='0' GROUP BY t.type_name ORDER BY remark DESC";
        return DbHelp.query(SQL, new BeanListHandler<>(Type.class, new BasicRowProcessor(new GenerousBeanProcessor())));
    }

    public List<Type> selectTypeListByMovieId(Integer movieId) {
        String SQL = "select t.* from type t inner join movie_type mt on t.type_id = mt.type_id where mt.movie_id = ?";
        return DbHelp.query(SQL, new BeanListHandler<>(Type.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieId);
    }
}
