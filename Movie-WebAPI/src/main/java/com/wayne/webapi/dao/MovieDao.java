package com.wayne.webapi.dao;

import com.wayne.webapi.entity.Movie;
import com.wayne.webapi.util.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class MovieDao {


    public Integer selectMovieCountByParams(Map<String,String> params) {
        String SQL = "SELECT COUNT(-1) FROM movie m";
        String append1 = " WHERE m.movie_name LIKE ?";

        String keys = params.get("keys");
        List<String> paramsList = new ArrayList<>();
        if(StringUtils.isNotEmpty(keys)) {
            keys = "%" + keys + "%";
            paramsList.add(keys);
            SQL = SQL + append1;
        }
        return DbHelp.query(SQL, new ScalarHandler<Long>(), paramsList.toArray()).intValue();
    }

    public List<Movie> selectMovieListByParams(Map<String,String> params) {
        String SQL = "SELECT * FROM movie m";
        String append1 = " WHERE m.movie_name LIKE ?";
        String append2 = " INNER JOIN movie_type mt ON m.movie_id = mt.movie_id WHERE mt.type_id = ?";
        String endSQL = " LIMIT ?,?";

        String keys = params.get("keys");
        String typeId = params.get("typeId");
        String pageNo = params.get("pageNo");
        String pageSize = params.get("pageSize");

        List<Object> paramsList = new ArrayList<>();
        if(StringUtils.isNotEmpty(keys)) {
             keys = "%" + keys + "%";
            paramsList.add(keys);
            SQL = SQL + append1;
        } else if(StringUtils.isNotEmpty(typeId)) {
            paramsList.add(typeId);
            SQL = SQL + append2;
        }
        SQL = SQL + endSQL;
        Integer startNo = (Integer.valueOf(pageNo) - 1) * Integer.valueOf(pageSize);
        paramsList.add(startNo);
        paramsList.add(Integer.valueOf(pageSize));

        return DbHelp.query(SQL, new BeanListHandler<>(Movie.class, new BasicRowProcessor(new GenerousBeanProcessor())), paramsList.toArray());
    }

    public List<Movie> selectMovieByRanking() {
        String SQL = "select * from movie order by page_view desc limit 0, 10";
        return DbHelp.query(SQL, new BeanListHandler<>(Movie.class, new BasicRowProcessor(new GenerousBeanProcessor())));
    }

    public Movie selectMovieByMovieId(Integer movieId) {
        String SQL = "select * from movie where movie_id = ?";
        return DbHelp.query(SQL, new BeanHandler<>(Movie.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieId);

    }

    public void updateMoviePageViewById(Movie movie) {
        String SQL = "update movie set page_view = ? where movie_id = ?";
        DbHelp.update(SQL, movie.getPageView(), movie.getMovieId());
    }

    public void updateMovieReplyNum(Movie movie) {
        String SQL = "update movie set reply_num = ? where movie_id = ?";
        DbHelp.update(SQL, movie.getReplyNum(), movie.getMovieId());
    }
}
