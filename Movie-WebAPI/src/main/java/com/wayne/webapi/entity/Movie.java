package com.wayne.webapi.entity;

import java.util.Date;
import java.util.List;

/**
 * @author Wayne
 * @date 2018/12/25
 */
public class Movie {

    private Integer movieId;

    private String movieName;

    private String movieDirector;

    private String area;

    private String year;

    private String imgPath;

    private String movieIntro;

    private String simpoIntro;

    private Integer pageView;

    private Integer replyNum;

    private String isDelete;

    private Date createTime;

    private Date updateTime;

    private String remark;

    private List<Type> typeList;

    private List<MovieReply> movieReplyList;

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDirector() {
        return movieDirector;
    }

    public void setMovieDirector(String movieDirector) {
        this.movieDirector = movieDirector;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getMovieIntro() {
        return movieIntro;
    }

    public void setMovieIntro(String movieIntro) {
        this.movieIntro = movieIntro;
    }

    public String getSimpoIntro() {
        return simpoIntro;
    }

    public void setSimpoIntro(String simpoIntro) {
        this.simpoIntro = simpoIntro;
    }

    public Integer getPageView() {
        return pageView;
    }

    public void setPageView(Integer pageView) {
        this.pageView = pageView;
    }

    public Integer getReplyNum() {
        return replyNum;
    }

    public void setReplyNum(Integer replyNum) {
        this.replyNum = replyNum;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Type> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<Type> typeList) {
        this.typeList = typeList;
    }

    public List<MovieReply> getMovieReplyList() {
        return movieReplyList;
    }

    public void setMovieReplyList(List<MovieReply> movieReplyList) {
        this.movieReplyList = movieReplyList;
    }
}

