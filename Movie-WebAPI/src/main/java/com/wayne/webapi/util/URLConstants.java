package com.wayne.webapi.util;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class URLConstants {

    /**
     * 主页
     */
    public static final String URL_INDEX = "index";

    /**
     * 详情
     */
    public static final String URL_DETAIL = "detail";

    /**
     * 登录
     */
    public static final String URL_LOGIN = "login";

    /**
     * 注册
     */
    public static final String URL_REGISTER = "register";

}
