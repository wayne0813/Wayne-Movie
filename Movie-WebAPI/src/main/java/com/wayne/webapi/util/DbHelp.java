package com.wayne.webapi.util;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.SQLException;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class DbHelp {

    private static QueryRunner runner = new QueryRunner(ConnectionManager.getConnection());

    public static void update(String sql, Object... params) {
        try {
            runner.update(sql, params);
            System.out.println("[执行SQL: " + sql + "]");
            System.out.print("[SQL参数: ");
            for(Object obj : params) {
                System.out.print(obj + " --- ");
            }
            System.out.println("]");
        } catch (SQLException e) {
            System.out.println("[执行SQL: " + sql + "]");
            System.out.print("[SQL参数: ");
            for(Object obj : params) {
                System.out.print(obj + " --- ");
            }
            System.out.println("]");
            e.printStackTrace();
        }
    }

    public static <T> T query(String sql, ResultSetHandler<T> resultSetHandler, Object... params) {
        T t = null;
        try {


            t = runner.query(sql, resultSetHandler, params);
            System.out.println("[执行SQL: " + sql + "]");
            System.out.print("[SQL参数: ");
            for(Object obj : params) {
                System.out.print(obj + " --- ");
            }
            System.out.println("]");
        } catch (SQLException e) {
            System.out.println("[执行SQL: " + sql + "异常]");
            System.out.print("[SQL参数: ");
            for(Object obj : params) {
                System.out.print(obj + " --- ");
            }
            System.out.println("]");
            e.printStackTrace();
        }
        return t;
    }

    public static <T> T insert(String sql, ResultSetHandler<T> resultSetHandler, Object... params) {
        T t = null;
        try {
            t = runner.insert(sql, resultSetHandler, params);
            System.out.println("[执行SQL: " + sql + "]");
            System.out.print("[SQL参数: ");
            for(Object obj : params) {
                System.out.print(obj + " --- ");
            }
            System.out.println("]");
        } catch (SQLException e) {
            System.out.println("[执行SQL: " + sql + "]");
            System.out.print("[SQL参数: ");
            for(Object obj : params) {
                System.out.print(obj + " --- ");
            }
            System.out.println("]");
            e.printStackTrace();
        }
        return t;
    }


}
