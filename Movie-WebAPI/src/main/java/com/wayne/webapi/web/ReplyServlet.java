package com.wayne.webapi.web;

import com.wayne.webapi.dto.JsonResult;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2018/12/30
 */
@WebServlet("/api/reply.html")
public class ReplyServlet extends BaseServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        JsonResult jsonResult = replyService.addReply(req);
        sendJson(jsonResult, resp);
    }
}
