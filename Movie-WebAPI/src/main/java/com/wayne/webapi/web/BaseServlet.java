package com.wayne.webapi.web;

import com.google.gson.Gson;
import com.wayne.webapi.service.MovieService;
import com.wayne.webapi.service.ReplyService;
import com.wayne.webapi.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class BaseServlet extends HttpServlet {

    protected MovieService movieService = new MovieService();

    protected ReplyService replyService = new ReplyService();

    protected UserService userService = new UserService();

    protected void forWord(HttpServletRequest request, HttpServletResponse response, String URL) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/" + URL + ".jsp").forward(request, response);
    }

    protected void sendJson(Object data, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        writer.print(new Gson().toJson(data));
        writer.flush();
        writer.close();
    }

}
