package com.wayne.webapi.web;

import com.wayne.webapi.dto.JsonResult;
import com.wayne.webapi.util.URLConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/4
 */
@WebServlet("/api/register.html")
public class RegisterServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forWord(req, resp, URLConstants.URL_REGISTER);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = userService.register(req);
        sendJson(jsonResult, resp);
    }
}
