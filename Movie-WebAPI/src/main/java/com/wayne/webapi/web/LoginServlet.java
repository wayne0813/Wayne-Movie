package com.wayne.webapi.web;

import com.wayne.webapi.dto.JsonResult;
import com.wayne.webapi.util.URLConstants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/4
 */
@WebServlet("/api/login.html")
public class LoginServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        for(Cookie cookie :cookies) {
            if("mobile".equals(cookie.getName())) {
                req.setAttribute("mobile", cookie.getValue());
            }
            if("password".equals(cookie.getName())) {
                req.setAttribute("password", cookie.getValue());
            }
        }
        forWord(req, resp, URLConstants.URL_LOGIN);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = userService.login(req, resp);
        sendJson(jsonResult, resp);
    }
}
