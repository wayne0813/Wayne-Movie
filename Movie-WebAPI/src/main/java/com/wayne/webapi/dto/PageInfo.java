package com.wayne.webapi.dto;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class PageInfo {

    /**
     * 每页数量
     */
    public static final Integer PAGE_SIZE = 5;

    private Integer pageNo;

    private Integer total;

    private Integer pages;

    private Object data;

    private PageInfo() {
    }

    public static PageInfo startPage(Integer pageNo, Integer total) {
        Integer pages = total / PageInfo.PAGE_SIZE;
        pages = ((total % PageInfo.PAGE_SIZE) > 0 ? ++pages : pages);

        PageInfo pageInfo = new PageInfo();
        pageInfo.setPageNo(pageNo);
        pageInfo.setTotal(total);

        pageInfo.setPages(pages);
        return pageInfo;
    }


    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
