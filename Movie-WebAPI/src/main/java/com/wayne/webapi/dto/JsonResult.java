package com.wayne.webapi.dto;

import com.wayne.webapi.util.Config;

/**
 * @author Wayne
 * @date 2019/1/3
 */
public class JsonResult {

    private String state;
    private Object data;
    private String message;

    public static JsonResult success() {
        return success(null);
    }

    public static JsonResult success(Object data) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setData(data);
        jsonResult.setState(Config.STATE_SUCCESS);
        jsonResult.setMessage(Config.MESSAGE);
        return jsonResult;
    }

    public static JsonResult error(String message) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setState(Config.STATE_ERROR);
        jsonResult.setMessage(message);
        return jsonResult;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
