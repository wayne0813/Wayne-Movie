package com.wayne.webapi.service;

import com.wayne.webapi.dao.MovieReplyDao;
import com.wayne.webapi.dao.MovieDao;
import com.wayne.webapi.dao.TypeDao;
import com.wayne.webapi.dto.PageInfo;
import com.wayne.webapi.entity.Movie;
import com.wayne.webapi.entity.MovieReply;
import com.wayne.webapi.entity.Type;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class MovieService {
    private MovieDao movieDao = new MovieDao();
    private TypeDao typeDao =new TypeDao();
    private MovieReplyDao movieReplyDao = new MovieReplyDao();

    public void findAllMovieWithPage(HttpServletRequest request, HttpServletResponse response) {
        String pageNo = request.getParameter("pageNo");
        String keys = request.getParameter("keys");
        String typeId = request.getParameter("typeId");

        pageNo = StringUtils.isNumeric(pageNo) ? pageNo : "1";

        Map<String, String> params = new HashMap();
        params.put("keys", keys);
        params.put("typeId", typeId);
        params.put("pageNo", pageNo);
        params.put("pageSize", PageInfo.PAGE_SIZE.toString());

        Integer total = movieDao.selectMovieCountByParams(params);
        PageInfo pageInfo = PageInfo.startPage(Integer.valueOf(pageNo), total);

        List<Movie> movieList = movieDao.selectMovieListByParams(params);
        if(movieList != null && movieList.size() > 0) {
            for(Movie movie : movieList) {
                List<Type> typeList = typeDao.selectTypeListByMovieId(movie.getMovieId());
                movie.setTypeList(typeList);
            }
        }

        pageInfo.setData(movieList);

        List<Type> typeList = typeDao.selectAllType();

        List<Movie> rankingMovie = movieDao.selectMovieByRanking();

        request.setAttribute("pageInfo", pageInfo);
        request.setAttribute("rankingMovie", rankingMovie);
        request.setAttribute("typeList", typeList);
        request.setAttribute("keys", keys);
        request.setAttribute("typeId", typeId);
    }


    public void findMovieDetail(HttpServletRequest request, HttpServletResponse response) {
        String movieId = request.getParameter("movieId");
        boolean flag = true;
        Movie movie = null;
        List<MovieReply> firstMovieReplyList = null;

        if(StringUtils.isEmpty(movieId) || !StringUtils.isNumeric(movieId)) {
            flag = false;
        }

        if(flag) {
            movie = movieDao.selectMovieByMovieId(Integer.valueOf(movieId));
        }

        if(movie == null) {
            flag = false;
        }

        if(flag) {
            movie.setPageView(movie.getPageView() + 1);
            movieDao.updateMoviePageViewById(movie);
            List<Type> typeList = typeDao.selectTypeListByMovieId(movie.getMovieId());
            movie.setTypeList(typeList);

            firstMovieReplyList = movieReplyDao.selectMovieReplyByMovieId(movie.getMovieId());
        }

        List<Type> headTypeList = typeDao.selectAllType();

        request.setAttribute("flag", flag);
        request.setAttribute("movie", movie);
        request.setAttribute("typeList", headTypeList);
        request.setAttribute("firstMovieReplyList", firstMovieReplyList);
    }
}
