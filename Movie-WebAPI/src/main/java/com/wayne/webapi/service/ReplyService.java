package com.wayne.webapi.service;

import com.wayne.webapi.dao.MovieReplyDao;
import com.wayne.webapi.dto.JsonResult;
import com.wayne.webapi.entity.Movie;
import com.wayne.webapi.entity.UserInfo;
import com.wayne.webapi.dao.MovieDao;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Wayne
 * @date 2018/12/30
 */
public class ReplyService {

    private MovieDao movieDao = new MovieDao();
    private MovieReplyDao movieReplyDao = new MovieReplyDao();

    public JsonResult addReply(HttpServletRequest req) {
        UserInfo userInfo = (UserInfo) req.getSession().getAttribute("userInfo");

        String movieId = req.getParameter("movieId");
        String replyContent = req.getParameter("replyContent");

        if(movieId == null || replyContent == null) {
            System.out.println("参数异常-01");
            return JsonResult.error("参数异常!");
        }

        Movie movie = movieDao.selectMovieByMovieId(Integer.valueOf(movieId));

        if(movie == null) {
            System.out.println("参数异常-02");
            return JsonResult.error("参数异常!");
        }

        movie.setReplyNum(movie.getReplyNum() + 1);
        movieDao.updateMovieReplyNum(movie);
        movieReplyDao.insertMovieReply(movieId, userInfo.getUserId().toString(), replyContent);

        System.out.println("处理成功");
        return JsonResult.success();
    }

}
