package com.wayne.webapi.service;

import com.wayne.webapi.dao.UserDao;
import com.wayne.webapi.dto.JsonResult;
import com.wayne.webapi.entity.UserInfo;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class UserService {

    private UserDao userDao = new UserDao();

    public JsonResult login(HttpServletRequest req, HttpServletResponse resp) {
        String mobile = req.getParameter("mobile");
        String password = req.getParameter("password");
        String remember = req.getParameter("remember");

        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) {
            return JsonResult.error("参数异常,请校验后重试!");
        }

        UserInfo userInfo = userDao.selectUserByMobileAndPassword(mobile, password);

        if(userInfo == null) {
            return JsonResult.error("账号或密码错误!");
        }

        HttpSession session = req.getSession();
        session.setAttribute("userInfo", userInfo);

        Cookie usernameCookie = new Cookie("mobile", mobile);
        usernameCookie.setMaxAge(60*60*24*7);
        usernameCookie.setDomain("localhost");
        usernameCookie.setPath("/");
        usernameCookie.setHttpOnly(true);
        resp.addCookie(usernameCookie);

        if(StringUtils.isNotEmpty(remember)) {
            Cookie passwordCookie = new Cookie("password", password);
            passwordCookie.setMaxAge(60*60*24*7);
            passwordCookie.setHttpOnly(true);
            passwordCookie.setPath("/");
            passwordCookie.setDomain("localhost");
            resp.addCookie(passwordCookie);
        } else {
            System.out.println("清除密码");
            Cookie[] cookies = req.getCookies();
            for(Cookie cookie : cookies) {
                if("password".equals(cookie.getName())) {
                    cookie.setMaxAge(0);
                    cookie.setHttpOnly(true);
                    cookie.setPath("/");
                    cookie.setDomain("localhost");
                    resp.addCookie(cookie);
                    break;
                }
            }
        }
        return JsonResult.success("登录成功!");
    }

    public JsonResult register(HttpServletRequest req) {
        String userName = req.getParameter("username");
        String nickNam = req.getParameter("pickname");
        String password = req.getParameter("password");
        String mobile = req.getParameter("mobile");
        // String email = req.getParameter("email");
        String kaptcha = req.getParameter("kaptcha");

        String kaptchaExpected = (String) req.getSession().getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);

        if(!kaptchaExpected.equals(kaptcha)) {
            return JsonResult.error("验证码错误!");
        }

        UserInfo userInfo = new UserInfo();
        userInfo.setUserName(userName);
        userInfo.setNickName(nickNam);
        userInfo.setMobile(mobile);
        userInfo.setPassword(password);

        Integer id = userDao.saveNewUserInfo(userInfo);

        UserInfo user = userDao.selectUserById(id);

        HttpSession session = req.getSession();
        session.setAttribute("userInfo", user);

        Cookie mobileCookie = new Cookie("mobile", user.getMobile());
        mobileCookie.setDomain("localhost");
        mobileCookie.setMaxAge(60*60*24*7);
        mobileCookie.setHttpOnly(true);
        mobileCookie.setPath("/");

        return JsonResult.success();
    }
}
