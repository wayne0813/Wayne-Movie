<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>首页</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <%@ include file="include/css.jsp"%>

  <style>
    body {
      margin-top: 60px;
    }
  </style>
</head>

<body>
  <%@ include file="include/header.jsp"%>
  <!-- 文章列表开始 -->
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <!-- 显示电影内容↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
        <c:if test="${empty pageInfo.data}">
          <h3>找不到该资源,换个关键字试试</h3>
        </c:if>
        <c:forEach items="${pageInfo.data}" var="movie">
          <div class="article-span">
            <div class="media article">
              <div class="media-body">
                <a href="/api/detail.html?movieId=${movie.movieId}"><span class="media-heading">${movie.movieName}</span></a>
                <span class="">${movie.movieDirector}</span>
                <p class="">${movie.simpoIntro}</p>

                <div class="meta">
                  <c:forEach items="${movie.typeList}" var="type" varStatus="vs">
                    <span class="label
                      <c:choose>
					    <c:when test="${vs.count==1}">
                          label-default
                        </c:when>
                        <c:when test="${vs.count == 2}">
                          label-primary
                        </c:when>
                        <c:otherwise>
                          label-info
                        </c:otherwise>
                      </c:choose>
                      ">${type.typeName}
                    </span>
                  </c:forEach>
                </div>

              </div>
              <div class="media-right">
                <a href="javascript:;"> <img src="${movie.imgPath}" style="width: 99px; height: 128px" class="media-object" alt=""></a>
              </div>
            </div>
          </div>
        </c:forEach>
        <div class="text-center">
          <ul id="pagination" class="pagination pagination-lg"></ul>
        </div>
      </div>

      <!-- 显示浏览排行↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ -->
      <div class="col-md-3">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">浏览排行</h3>
          </div>

          <ul class="list-group text-primary">
            <c:forEach items="${rankingMovie}" var="movie" varStatus="vs">
              <li class="list-group-item">
                <a href="/api/detail.html?movieId=${movie.movieId}">${vs.count}.${movie.movieName} </a>
                <label class="label
                  <c:choose>
                    <c:when test="${vs.count == 1}">
                      label-danger
                    </c:when>
                    <c:when test="${vs.count == 2}">
                      label-warning
                    </c:when>
                    <c:when test="${vs.count == 3}">
                      label-info
                    </c:when>
                    <c:otherwise>
                      label-default
                    </c:otherwise>
                  </c:choose>
                ">${movie.pageView}
              </label>
              </li>
            </c:forEach>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <%@ include file="include/js.jsp"%>

  <script>
    $(function () {
        var keys = "${param.keys}";
        var typeId = "${param.typeId}";
        var pages = "${pageInfo.pages}";
        keys = encodeURIComponent(keys);
        typeId = encodeURIComponent(typeId);

        $("#pagination").twbsPagination({
            totalPages: pages,
            visiblePages: 3,
            href : "/api/index.html?pageNo={{number}}&keys=" + keys + "&typeId=" + typeId,
            first: "<<",
            prev: "<",
            next: ">",
            last: ">>"
        });
    });
  </script>
</body>

</html>