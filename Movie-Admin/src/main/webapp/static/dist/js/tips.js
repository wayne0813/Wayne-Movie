$(function(){
	var tips = function(){
		$.post("/admin/tips.html", function(res){
			if(res.state == "success"){
				$("#unReadCount").text(res.data);
				layer.tips('<a href="/admin/replyList.html">有' + res.data + '条新评论需要您审核</a>', '#unReadCount', {
					tips : [3, '#e4e4e4' ]
				});
			}
		});
	};
	
	setInterval(function(){
		tips();
	}, 10 * 1000);
	
	tips();
})