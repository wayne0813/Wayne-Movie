<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>电影列表</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <%@ include file="../include/css.jsp"%>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <%@ include file="../include/header.jsp"%>
    <%@ include file="../include/left.jsp"%>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">

            <form action="/admin/movieList.html" class="form-inline pull-left">
              <input type="text" class="form-control" name="keys" value="${param.keys}" id="keys" placeholder="关键字" />
              <button class="btn btn-primary"><i class="fa fa-search"></i></button>
            </form>

            <a href="/admin/saveMovie.html" class="btn btn-success pull-right">新增电影</a>
          </div>

          <div class="box-body">

            <table class="table" id="cust_table">
              <thead>
                <c:if test="${empty pageInfo.data}">
                  <h3>找不到该资源</h3>
                </c:if>
                <c:if test="${not empty pageInfo.data}">
                  <tr>
                    <th>名称</th>
                    <th>导演</th>
                    <th>类型</th>
                    <th>上映年份</th>
                    <th>地区</th>
                    <th>操作</th>
                  </tr>
                </c:if>
              </thead>

              <tbody>

                <c:forEach items="${pageInfo.data}" var="movie">
                  <tr>
                    <td>${movie.movieName}</td>
                    <td>${movie.movieDirector}</td>
                    <td>
                      <c:forEach items="${movie.typeList}" var="type" varStatus="vs">
                        ${type.typeName}
                        <c:if test="${not vs.last}">/</c:if>
                      </c:forEach>
                    </td>
                    <td>${movie.year}</td>
                    <td>${movie.area}</td>
                    <td>
                      <a class="delete" filmName="${movie.movieName}" rel="${movie.movieId}" href="javascript:;">删除</a>
                      /
                      <a href="/admin/editMovie.html?movieId=${movie.movieId}">修改</a>
                    </td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
            <br>
            <ul id="pagination" class="pagination pull-right"></ul>
          </div>
        </div>
      </section>
    </div>
    <%@ include file="../include/footer.jsp"%>
  </div>
  <%@ include file="../include/js.jsp"%>
  <script>
    $(function () {
      $(".delete").click(function () {
        var id = $(this).attr("rel");
        var filmName = $(this).attr("filmName");
        /* 灵感来自于GitHub */
        layer.prompt({ title: '请输入要删除的电影名,并确认', formType: 3 }, function (text, index) {
          if (text == filmName) {
            layer.close(index);
            window.location.href = "/admin/deleteMovie.html?movieId=" + id;
            layer.msg('删除成功');
          } else {
            layer.close(index);
            layer.msg('电影名输入错误,删除已取消!');
          }
        });
      });
      var keys = "${param.keys}";
      key = encodeURIComponent(keys);
      $("#pagination").twbsPagination({
        totalPages: "${pageInfo.pages}",
        visiblePages: 3,
        href: "/admin/movieList.html?pageNo={{number}}&keys=" + key,
        first: "首页",
        prev: "上一页",
        next: "下一页",
        last: "末页"
      });
    });
  </script>
</body>

</html>