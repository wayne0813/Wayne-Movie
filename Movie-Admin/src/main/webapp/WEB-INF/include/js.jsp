<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- jQuery 2.2.3 -->
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/static/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/static/dist/js/app.min.js"></script>
<script src="/static/dist/js/jquery.validate.min.js"></script>
<script src="/static/plugins/layer/layer.js"></script>

<!-- DataTables -->
<script src="/static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- page -->
<script src="/static/dist/js/jquery.twbsPagination.min.js"></script>

<script src="/static/plugins/editer/scripts/module.min.js"></script>
<script src="/static/plugins/editer/scripts/hotkeys.min.js"></script>
<script src="/static/plugins/editer/scripts/uploader.min.js"></script>
<script src="/static/plugins/editer/scripts/simditor.min.js"></script>
<!-- 下拉框筛选插件 -->
<script src="/static/plugins/select2/select2.min.js"></script>
<script src="/static/plugins/select2/select2.full.min.js"></script>
<!--引入webuploader JS-->
<script src="/static/plugins/uploader/webuploader.js"></script>
<!-- tips显示层控制js文件 -->
<script src="/static/dist/js/tips.js"></script>
