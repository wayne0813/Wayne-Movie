package com.wayne.admin.filter;

import com.wayne.admin.utils.LogTemp;
import com.wayne.admin.entity.SystemAdmin;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class LoginFilter implements Filter {

    private List<String> URIList;

    @Override
    public void init(FilterConfig config) {
        URIList = Arrays.asList(config.getInitParameter("paths").split(";"));

        LogTemp.printLog();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String requestURI = req.getRequestURI();

        if(isFilter(requestURI)) {
            SystemAdmin admin = (SystemAdmin) req.getSession().getAttribute("admin");
            if(admin != null) {
                chain.doFilter(req, resp);
            } else {
                // resp.setHeader("REDIRECT", "/api/login.html?path=" + requestURI + "?movieId=" + movieId);
                // resp.setHeader("REDIRECT", "/admin/login.html");
                resp.sendRedirect("/admin/login.html");
            }
        } else {
            chain.doFilter(req, resp);
        }
    }

    private Boolean isFilter(String requestURI) {
        if(requestURI == null) {
            return false;
        }
        for(String uri : URIList) {
            if(requestURI.startsWith(uri)) {
                return true;
            }
        }
        return false;
    }









}
