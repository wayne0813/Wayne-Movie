package com.wayne.admin.service;

import com.wayne.admin.dao.AdminDao;
import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.entity.SystemAdmin;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class AdminService {

    private AdminDao adminDao = new AdminDao();

    public JsonResult login(HttpServletRequest req, HttpServletResponse resp) {
        String adminName = req.getParameter("adminName");
        String password = req.getParameter("password");
        String remember = req.getParameter("remember");

        if(StringUtils.isEmpty(adminName) || StringUtils.isEmpty(password)) {
            return JsonResult.error("参数异常");
        }

        SystemAdmin admin = adminDao.selectAdminByAdminNameAndPassword(adminName, password);

        if(admin == null) {
            return JsonResult.error("账户或密码错误!");
        }

        req.getSession().setAttribute("admin", admin);

        Cookie adminNameCookie = new Cookie("adminName", admin.getAdminName());
        adminNameCookie.setDomain("localhost");
        adminNameCookie.setPath("/");
        adminNameCookie.setMaxAge(60*60*24*7);
        adminNameCookie.setHttpOnly(true);
        resp.addCookie(adminNameCookie);

        return JsonResult.success();
    }

    public void logout(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().removeAttribute("admin");
    }
}
