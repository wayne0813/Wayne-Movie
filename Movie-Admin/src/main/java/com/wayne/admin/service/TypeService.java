package com.wayne.admin.service;

import com.wayne.admin.dao.TypeDao;
import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.dto.PageInfo;
import com.wayne.admin.entity.MovieType;
import com.wayne.admin.entity.Type;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Wayne
 * @date 2019/1/7
 */
public class TypeService {

    private TypeDao typeDao = new TypeDao();

    public JsonResult findTypeList(HttpServletRequest req, HttpServletResponse resp) {
        String movieId = req.getParameter("movieId");
        List<Type> typeList = typeDao.selectTypeList();

        List<MovieType> movieTypeList = null;
        if(movieId != null) {
            movieTypeList = typeDao.selectMovieTypeByMovieId(movieId);
        }

        if(movieTypeList != null) {
            for(Type type : typeList) {
                for(MovieType movieType : movieTypeList) {
                    if(type.getTypeId().equals(movieType.getTypeId())) {
                        type.setSelected(true);
                        continue;
                    }
                }
            }
        }

        return JsonResult.success(typeList);
    }

    public void findAllTypePage(HttpServletRequest req, HttpServletResponse resp) {
        String pageNo = req.getParameter("pageNo");
        int total = typeDao.countTypeTotal();
        pageNo = StringUtils.isNumeric(pageNo) ? pageNo : "1";
        PageInfo pageInfo = PageInfo.startPage(Integer.valueOf(pageNo), total);
        List<Type> typeList = typeDao.selectTypeListPage(Integer.valueOf(pageNo), PageInfo.PAGE_SIZE);
        pageInfo.setData(typeList);
        req.setAttribute("pageInfo", pageInfo);
    }

    public JsonResult saveType(HttpServletRequest req, HttpServletResponse resp) {
        String typeId = req.getParameter("typeId");
        String typeName = req.getParameter("typeName");

        if(typeId == null) {
            typeDao.insertType(typeName);
        } else {
            typeDao.updateTypeById(typeName, typeId);
        }

        return JsonResult.success();
    }


    public JsonResult deleteType(HttpServletRequest req, HttpServletResponse resp) {
        String id = req.getParameter("id");
        List<MovieType> movieTypeList = typeDao.selectMovieTypeByTypeId(Integer.valueOf(id));
        if(movieTypeList == null) {
            return JsonResult.error("该类型下有电影,无法删除!");
        }
        typeDao.deleteTypeByTypeId(Integer.valueOf(id));
        return JsonResult.success();
    }
}
