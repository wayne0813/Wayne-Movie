package com.wayne.admin.service;

import com.wayne.admin.dao.ReplyDao;
import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.dto.PageInfo;
import com.wayne.admin.entity.MovieReply;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author Wayne
 * @date 2019/1/8
 */
public class ReplyService {
    private ReplyDao replyDao = new ReplyDao();

    public void toReplyListPage(HttpServletRequest req, HttpServletResponse resp) {
        String pageNo = req.getParameter("pageNo");
        pageNo = StringUtils.isNumeric(pageNo) ? pageNo : "1";

        int total = replyDao.selectCountReply();
        PageInfo pageInfo = PageInfo.startPage(Integer.valueOf(pageNo), total);
        List<MovieReply> movieReplyList = replyDao.selectReplyListPage(Integer.valueOf(pageNo), PageInfo.PAGE_SIZE);
        pageInfo.setData(movieReplyList);

        req.setAttribute("pageInfo", pageInfo);
    }

    public JsonResult editState(HttpServletRequest req, HttpServletResponse resp) {
        String state = req.getParameter("state");
        String replyId = req.getParameter("replyId");
        if("0".equals(state)) {
            replyDao.isOk(replyId);
        } else {
            replyDao.notOk(replyId);
        }
        return JsonResult.success();
    }

    public JsonResult countReply() {
        Integer countReply = replyDao.countReply();
        return JsonResult.success(countReply);
    }
}
