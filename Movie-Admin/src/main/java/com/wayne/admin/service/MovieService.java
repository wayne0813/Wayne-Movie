package com.wayne.admin.service;

import com.wayne.admin.dao.CommentsDao;
import com.wayne.admin.dao.MovieDao;
import com.wayne.admin.dao.TypeDao;
import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.dto.PageInfo;
import com.wayne.admin.entity.Movie;
import com.wayne.admin.entity.Type;
import com.wayne.admin.utils.Config;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class MovieService {

    private CommentsDao commentsDao = new CommentsDao();
    private MovieDao movieDao = new MovieDao();
    private TypeDao typeDao = new TypeDao();

    public void findAllMovieWithPage(HttpServletRequest req, HttpServletResponse resp) {
        String keys = req.getParameter("keys");
        String pageNo = req.getParameter("pageNo");

        pageNo = StringUtils.isNumeric(pageNo) ? pageNo : "1";

        Map<String, String> params = new HashMap();
        params.put("keys", keys);
        params.put("pageNo", pageNo);
        params.put("pageSize", PageInfo.PAGE_SIZE.toString());

        Integer total = movieDao.countAllMovie(keys);

        PageInfo pageInfo = PageInfo.startPage(Integer.valueOf(pageNo), total);
        List<Movie> movieList = movieDao.selectMovieListByParams(params);
        if(movieList != null && movieList.size() > 0) {
            for(Movie movie : movieList) {
                List<Type> typeList = typeDao.selectTypeListByMovieId(movie.getMovieId());
                movie.setTypeList(typeList);
            }
        }

        pageInfo.setData(movieList);

        req.setAttribute("pageInfo", pageInfo);
        req.setAttribute("keys", keys);
    }

    public Boolean checkMovieName(HttpServletRequest req, HttpServletResponse resp) {
        String movieName = req.getParameter("movieName");
        String movieId = req.getParameter("movieId");
        Movie tempMovie = movieDao.selectMovieByMovieId(movieId);
        Movie movie = movieDao.selectMovieByMovieName(movieName);

        if(tempMovie != null) {
            if(tempMovie.getMovieName().equals(movie.getMovieName())) {
                return true;
            }
        }
        return movie == null;
    }

    public JsonResult saveMovie(HttpServletRequest req, HttpServletResponse resp) {
        String movieName = req.getParameter("movieName");
        String movieDirector = req.getParameter("director");
        String area = req.getParameter("area");
        String year = req.getParameter("year");
        String[] types = req.getParameterValues("type");
        String imgPath = req.getParameter("imageName");
        String movieIntro = req.getParameter("content");

        Movie movie = new Movie();
        movie.setMovieName(movieName);
        movie.setMovieDirector(movieDirector);
        movie.setArea(area);
        movie.setYear(year);
        movie.setImgPath(imgPath);
        movie.setMovieIntro(movieIntro);

        Integer movieId = movieDao.insertMovie(movie);

        List<String> typeList = Arrays.asList(types);
        for(String type : typeList) {
            Integer typeId = Integer.valueOf(type);
            typeDao.insertMovieType(movieId, typeId);
        }

        return JsonResult.success();
    }

    public void toEditMoviePage(HttpServletRequest req, HttpServletResponse resp) {
        String movieId = req.getParameter("movieId");
        Movie movie = movieDao.selectMovieByMovieId(movieId);
        movie.setImgPath(Config.IMG_PATH + "/" + movie.getImgPath());
        req.setAttribute("movie", movie);
    }

    public JsonResult editMovie(HttpServletRequest req, HttpServletResponse resp) {
        String movieId = req.getParameter("id");
        String movieName = req.getParameter("movieName");
        String movieDirector = req.getParameter("director");
        String area = req.getParameter("area");
        String year = req.getParameter("year");
        String[] types = req.getParameterValues("type");
        String imgPath = req.getParameter("imageName");
        String movieIntro = req.getParameter("content");

        imgPath = imgPath.substring(imgPath.lastIndexOf("/"));

        Movie movie = new Movie();
        movie.setMovieId(Integer.parseInt(movieId));
        movie.setMovieName(movieName);
        movie.setMovieDirector(movieDirector);
        movie.setArea(area);
        movie.setYear(year);
        movie.setImgPath(imgPath);
        movie.setMovieIntro(movieIntro);

        movieDao.editMovie(movie);
        typeDao.deleteMovieTypeByMovieId(movieId);

        List<String> typeList = Arrays.asList(types);
        for(String type : typeList) {
            Integer typeId = Integer.valueOf(type);
            typeDao.insertMovieType(Integer.valueOf(movieId), typeId);
        }

        return JsonResult.success();
    }

    public JsonResult deleteMovieById(HttpServletRequest req, HttpServletResponse resp) {
        String movieId = req.getParameter("movieId");
        movieDao.deleteMovieById(movieId);
        typeDao.deleteMovieTypeByMovieId(movieId);
        commentsDao.deleteCommentsByMovieId(movieId);

        return JsonResult.success();
    }
}
