package com.wayne.admin.dao;

import com.wayne.admin.utils.DbHelp;
import com.wayne.admin.entity.SystemAdmin;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class AdminDao {

    public SystemAdmin selectAdminByAdminNameAndPassword(String adminName, String password) {
        String SQL = "select * from system_admin where admin_name = ? and password = ?";
        return DbHelp.query(SQL, new BeanHandler<>(SystemAdmin.class, new BasicRowProcessor(new GenerousBeanProcessor())), adminName, password);
    }

}
