package com.wayne.admin.dao;

import com.wayne.admin.entity.MovieReply;
import com.wayne.admin.utils.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.util.List;

/**
 * @author Wayne
 * @date 2019/1/8
 */
public class ReplyDao {

    public int selectCountReply() {
        String SQL = "select count(-1) from movie_reply where is_delete = 0 and reply_state = 0";
        return DbHelp.query(SQL, new ScalarHandler<Long>()).intValue();
    }

    public List<MovieReply> selectReplyListPage(Integer pageNo, Integer pageSize) {
        Integer startPage = (pageNo-1)*pageSize;
        String SQL ="select mr.*, ui.nick_name as 'reviewerName', movie.movie_name from movie_reply mr left join user_info ui on ui.user_id = mr.reviewer_id left join movie on movie.movie_id = mr.movie_id where mr.reply_state = 0 and mr.is_delete = 0 limit ?, ?";
        return DbHelp.query(SQL, new BeanListHandler<>(MovieReply.class, new BasicRowProcessor(new GenerousBeanProcessor())), startPage, pageSize);
    }

    public void isOk(String replyId) {
        String SQL = "update movie_reply set reply_state = 1 where reply_id=?";
        DbHelp.update(SQL, replyId);
    }

    public void notOk(String replyId) {
        String SQL = "update movie_reply set reply_state = 1, is_delete = 1 where reply_id = ?";
        DbHelp.update(SQL, replyId);
    }

    public Integer countReply() {
        String SQL = "select count(-1) from movie_reply where is_delete = 0 and reply_state = 0";
        return DbHelp.query(SQL, new ScalarHandler<Long>()).intValue();
    }
}
