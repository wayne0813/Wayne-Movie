package com.wayne.admin.dao;

import com.wayne.admin.entity.Movie;
import com.wayne.admin.utils.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class MovieDao {

    public Integer countAllMovie(String keys) {
        String SQL = "select count(-1) from movie";
        List<String> param = new ArrayList<>();
        if(keys != null) {
            keys = "%" + keys + "%";
            param.add(keys);
            SQL = SQL + " where movie_name like ?";
        }
        return DbHelp.query(SQL, new ScalarHandler<Long>(), param.toArray()).intValue();
    }

    public List<Movie> selectMovieListByParams(Map<String,String> params) {
        String SQL = "SELECT * FROM movie m";
        String append = " WHERE m.movie_name LIKE ?";
        String endSQL = " LIMIT ?,?";

        String keys = params.get("keys");
        String pageNo = params.get("pageNo");
        String pageSize = params.get("pageSize");

        List<Object> paramsList = new ArrayList<>();
        if(StringUtils.isNotEmpty(keys)) {
            keys = "%" + keys + "%";
            paramsList.add(keys);
            SQL = SQL + append;
        }
        SQL = SQL + endSQL;
        Integer startNo = (Integer.valueOf(pageNo) - 1) * Integer.valueOf(pageSize);
        paramsList.add(startNo);
        paramsList.add(Integer.valueOf(pageSize));

        return DbHelp.query(SQL, new BeanListHandler<>(Movie.class, new BasicRowProcessor(new GenerousBeanProcessor())), paramsList.toArray());
    }

    public Movie selectMovieByMovieName(String movieName) {
        String SQL = "select * from movie where movie_name = ?";
        return DbHelp.query(SQL, new BeanHandler<>(Movie.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieName);
    }

    public Integer insertMovie(Movie movie) {
        String SQL = "insert into movie (movie_name, movie_director, area, year, img_path, movie_intro) values (?, ?, ?, ?, ?, ?)";
        return DbHelp.insert(SQL, new ScalarHandler<Long>(), movie.getMovieName(), movie.getMovieDirector(), movie.getArea(), movie.getYear(), movie.getImgPath(), movie.getMovieIntro()).intValue();
    }

    public Movie selectMovieByMovieId(String movieId) {
        String SQL = "select * from movie where movie_id = ?";
        return DbHelp.query(SQL, new BeanHandler<>(Movie.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieId);

    }

    public void editMovie(Movie movie) {
        String SQL = "update movie set movie_name=?, movie_director=?, area=?, year=?, img_path=?, movie_intro=? where movie_id=?";
        DbHelp.update(SQL, movie.getMovieName(), movie.getMovieDirector(), movie.getArea(), movie.getYear(), movie.getImgPath(), movie.getMovieIntro(), movie.getMovieId());
    }

    public void deleteMovieById(String movieId) {
        String SQL = "delete from movie where movie_id=?";
        DbHelp.update(SQL, movieId);
    }
}
