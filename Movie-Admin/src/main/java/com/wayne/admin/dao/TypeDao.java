package com.wayne.admin.dao;

import com.wayne.admin.entity.MovieType;
import com.wayne.admin.entity.Type;
import com.wayne.admin.utils.DbHelp;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.util.List;

/**
 * @author Wayne
 * @date 2019/1/5
 */
public class TypeDao {

    public List<Type> selectTypeListByMovieId(Integer movieId) {
        String SQL = "select t.* from type t inner join movie_type mt on t.type_id = mt.type_id where mt.movie_id = ?";
        return DbHelp.query(SQL, new BeanListHandler<>(Type.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieId);
    }

    public List<Type> selectTypeList() {
        String SQL = "select * from type";
        return DbHelp.query(SQL, new BeanListHandler<>(Type.class, new BasicRowProcessor(new GenerousBeanProcessor())));
    }

    public void insertMovieType(Integer movieId, Integer typeId) {
        String SQL = "insert into movie_type (movie_id, type_id) values (?, ?)";
        DbHelp.update(SQL, movieId, typeId);
    }

    public List<MovieType> selectMovieTypeByMovieId(String movieId) {
        String SQL = "select *from movie_type where movie_id = ?";
        return DbHelp.query(SQL, new BeanListHandler<>(MovieType.class, new BasicRowProcessor(new GenerousBeanProcessor())), movieId);
    }

    public void deleteMovieTypeByMovieId(String movieId) {
        String SQL = "delete from movie_type where movie_id=?";
        DbHelp.update(SQL, movieId);
    }

    public int countTypeTotal() {
        String SQL = "select count(-1) from type";
        return DbHelp.query(SQL, new ScalarHandler<Long>()).intValue();
    }

    public List<Type> selectTypeListPage(Integer pageNo, Integer pageSize) {
        int startPage = (pageNo-1)*pageSize;
        String SQl = "select * from type limit ?, ?";
        return DbHelp.query(SQl, new BeanListHandler<>(Type.class, new BasicRowProcessor(new GenerousBeanProcessor())), startPage, pageSize);

    }

    public void insertType(String typeName) {
        String SQL = "insert into type (type_name) values (?)";
        DbHelp.update(SQL, typeName);
    }

    public void updateTypeById(String typeName, String typeId) {
        String SQl = "update type set type_name=? where type_id=?";
        DbHelp.update(SQl, typeName, typeId);
    }

    public List<MovieType> selectMovieTypeByTypeId(Integer typeId) {
        String SQL = "select * from movie_type where type_id = ?";
        return DbHelp.query(SQL, new BeanListHandler<>(MovieType.class, new BasicRowProcessor(new GenerousBeanProcessor())), typeId);
    }

    public void deleteTypeByTypeId(Integer typeId) {
        String SQL = "delete from type where type_id = ?";
        DbHelp.update(SQL, typeId);
    }
}
