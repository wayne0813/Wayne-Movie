package com.wayne.admin.dao;

import com.wayne.admin.utils.DbHelp;

/**
 * @author Wayne
 * @date 2019/1/7
 */
public class CommentsDao {
    public void deleteCommentsByMovieId(String movieId) {
        String SQL = "update movie_reply set is_delete=1 where movie_id=?";
        DbHelp.update(SQL, movieId);
    }
}
