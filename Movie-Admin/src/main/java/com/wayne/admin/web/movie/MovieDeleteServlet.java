package com.wayne.admin.web.movie;

import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.web.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/7
 */
@WebServlet("/admin/deleteMovie.html")
public class MovieDeleteServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = movieService.deleteMovieById(req, resp);
        sendJson(jsonResult, resp);
    }
}
