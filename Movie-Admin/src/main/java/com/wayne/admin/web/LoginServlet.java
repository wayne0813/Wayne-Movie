package com.wayne.admin.web;

import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.utils.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/4
 */
@WebServlet("/admin/login.html")
public class LoginServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forWord(req, resp, URLConstant.URL_LOGIN);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult =  adminService.login(req, resp);
        sendJson(jsonResult, resp);
    }
}
