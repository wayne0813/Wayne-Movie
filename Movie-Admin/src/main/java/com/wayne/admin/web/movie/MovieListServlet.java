package com.wayne.admin.web.movie;

import com.wayne.admin.utils.URLConstant;
import com.wayne.admin.web.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/4
 */
@WebServlet("/admin/movieList.html")
public class MovieListServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        movieService.findAllMovieWithPage(req, resp);
        forWord(req, resp, URLConstant.URL_MOVIE_LIST);
    }

}
