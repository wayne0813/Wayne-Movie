package com.wayne.admin.web;

import com.google.gson.Gson;
import com.wayne.admin.service.AdminService;
import com.wayne.admin.service.MovieService;
import com.wayne.admin.service.ReplyService;
import com.wayne.admin.service.TypeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class BaseServlet extends HttpServlet {

    protected AdminService adminService = new AdminService();

    protected MovieService movieService = new MovieService();

    protected TypeService typeService = new TypeService();

    protected ReplyService replyService = new ReplyService();


    public void forWord(HttpServletRequest request, HttpServletResponse response, String URL) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/" + URL + ".jsp").forward(request, response);
    }

    public void sendJson(Object data, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter printWriterWriter = response.getWriter();
        printWriterWriter.print(new Gson().toJson(data));
        printWriterWriter.flush();
        printWriterWriter.close();
    }



}
