package com.wayne.admin.web;

import com.wayne.admin.utils.Config;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Wayne
 * @date 2019/1/7
 */
@WebServlet("/img/uploader")
@MultipartConfig
public class UploaderServlet extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        Part part = req.getPart("file");
        // 原始文件名
        String oldFileName = part.getHeader("Content-Disposition");
        String imgName = oldFileName.split(";")[2].split("\"")[1];
        String newImgName = UUID.randomUUID().toString() + imgName.substring(imgName.lastIndexOf("."));

        InputStream inputStream = part.getInputStream();
        FileOutputStream outputStream = new FileOutputStream(new File(Config.IMG_PATH, newImgName));
        IOUtils.copy(inputStream, outputStream);

        outputStream.flush();
        outputStream.close();
        inputStream.close();

        Map<String, Object> urlMap = new HashMap<>();
        urlMap.put("success", true);
        urlMap.put("file_path", Config.IMG_PATH + "/" + newImgName);
        urlMap.put("fileName", newImgName);

        sendJson(urlMap, resp);
    }
}
