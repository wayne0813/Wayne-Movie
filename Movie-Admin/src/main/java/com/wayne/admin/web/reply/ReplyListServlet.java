package com.wayne.admin.web.reply;

import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.utils.URLConstant;
import com.wayne.admin.web.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/8
 */
@WebServlet("/admin/replyList.html")
public class ReplyListServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        replyService.toReplyListPage(req, resp);
        forWord(req, resp, URLConstant.URL_REPLY_LIST);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = replyService.editState(req, resp);
        sendJson(jsonResult, resp);
    }
}
