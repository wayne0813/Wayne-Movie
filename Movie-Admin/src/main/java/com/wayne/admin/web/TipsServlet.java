package com.wayne.admin.web;

import com.wayne.admin.dto.JsonResult;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/8
 */
@WebServlet("/admin/tips.html")
public class TipsServlet extends BaseServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = replyService.countReply();
        sendJson(jsonResult, resp);
    }
}
