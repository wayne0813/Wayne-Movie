package com.wayne.admin.web.movie;


import com.wayne.admin.web.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/7
 */
@WebServlet("/admin/checkMovieName.html")
public class CheckNameServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Boolean isExist = movieService.checkMovieName(req, resp);
        sendJson(isExist, resp);
    }
}
