package com.wayne.admin.web.type;

import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.utils.URLConstant;
import com.wayne.admin.web.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/8
 */
@WebServlet("/admin/typeIndex.html")
public class TypeListServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        typeService.findAllTypePage(req, resp);
        forWord(req, resp, URLConstant.URL_TYPE_INDEX);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = typeService.saveType(req, resp);
        sendJson(jsonResult, resp);
    }

}
