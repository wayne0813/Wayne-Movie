package com.wayne.admin.web;

import com.wayne.admin.utils.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/8
 */
@WebServlet("/admin/logout.html")
public class LogOutServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        adminService.logout(req, resp);
        forWord(req, resp, URLConstant.URL_LOGIN);
    }
}
