package com.wayne.admin.web.type;

import com.wayne.admin.dto.JsonResult;
import com.wayne.admin.web.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Wayne
 * @date 2019/1/8
 */
@WebServlet("/admin/deleteType.html")
public class TypeDeleteServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        JsonResult jsonResult = typeService.deleteType(req, resp);
        sendJson(jsonResult, resp);
    }
}
