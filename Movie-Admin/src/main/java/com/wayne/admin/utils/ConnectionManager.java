package com.wayne.admin.utils;

import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;

/**
 * 数据库连接管理
 * @author Wayne
 * @date 2018/12/26
 */
public class ConnectionManager {

    public static BasicDataSource dataSource = new BasicDataSource();

    static {
        try {
            dataSource.setDriverClassName(Config.get("jdbc.driver"));
            dataSource.setUrl(Config.get("jdbc.url"));
            dataSource.setUsername(Config.get("jdbc.username"));
            dataSource.setPassword(Config.get("jdbc.password"));

            dataSource.setInitialSize(2);
            dataSource.setMaxIdle(3);
            dataSource.setMinIdle(1);
            dataSource.setMaxWaitMillis(5000);
        } catch (Exception e) {
            System.out.println("[ 数据库连接失败! ]");
            e.printStackTrace();
        }
    }

    public static DataSource getConnection() {
        return dataSource;
    }

}
