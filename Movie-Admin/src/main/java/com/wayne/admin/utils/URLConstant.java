package com.wayne.admin.utils;

/**
 * @author Wayne
 * @date 2019/1/4
 */
public class URLConstant {

    public static final String URL_LOGIN = "login";

    public static final String URL_MOVIE_LIST = "movieList";

    public static final String URL_MOVIE_ADD = "movieAdd";

    public static final String URL_MOVIE_EDIT = "movieEdit";

    public static final String URL_TYPE_INDEX = "typeIndex";

    public static final String URL_REPLY_LIST = "replyList";

}
