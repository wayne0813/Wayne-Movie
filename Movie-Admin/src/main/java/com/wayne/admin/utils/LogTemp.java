package com.wayne.admin.utils;

/**
 * @author Wayne
 * @date 2019/1/6
 */
public class LogTemp {

    public static void printLog() {
        System.out.println("                             _ooOoo_");
        System.out.println("                            o8888888o");
        System.out.println("                            88\" . \"88");
        System.out.println("                            (| -_- |)");
        System.out.println("                            O\\  =  /O");
        System.out.println("                         ____/`---'\\____");
        System.out.println("                       .'  \\\\|     |//  `.");
        System.out.println("                      /  \\\\|||  :  |||//  \\");
        System.out.println("                     /  _||||| -:- |||||-  \\");
        System.out.println("                     |   | \\\\\\  -  /// |   |");
        System.out.println("                     | \\_|  ''\\---/''  |   |");
        System.out.println("                     \\  .-\\__  `-`  ___/-. /");
        System.out.println("                   ___`. .'  /--.--\\  `. . __");
        System.out.println("               .\"\" '<  `.___\\_<|>_/___.'  >'\"\".");
        System.out.println("               | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |");
        System.out.println("               \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /");
        System.out.println("          ======`-.____`-.___\\_____/___.-`____.-'======");
        System.out.println("                             `=---='");
        System.out.println("          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        System.out.println("                     佛祖保佑        永无BUG");
    }

}
