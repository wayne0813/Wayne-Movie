package com.wayne.admin.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Wayne
 * @date 2018/12/26
 */
public class Config {

    public static final String STATE_SUCCESS = "success";
    public static final String STATE_ERROR = "error";
    public static final String MESSAGE = "操作成功!";

    public static final String IMG_PATH = "F:/IMG_PATH";


    /**
     * 读取.properties配置文件
     */
    public static String get(String key){
        Properties properties = new Properties();
        try {
            properties.load(Config.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            System.out.println("[ 配置文件加载失败! ]");
            e.printStackTrace();
        }
        return properties.getProperty(key);
    }

}
